import requests


def test_my_ip_request_status_is_200():
    response = requests.get("https://api.ipify.org/?format=json")
    assert response.status_code == 200


def test_my_ip_request_has_a_body():
    response = requests.get("https://api.ipify.org/?format=json")
    data = response.json()
    assert data is not None
    assert data['ip'] is not None


def test_my_ip_has_json_header():
    response = requests.get("https://api.ipify.org/?format=json")
    assert response.headers["Content-Type"] == "application/json"
