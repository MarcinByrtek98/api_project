import pytest
import requests


@pytest.fixture(scope='module')
def my_ip():
    print("Request")
    url = "https://api.ipify.org/?format=json"
    response = requests.get(url)
    return response
def test_my_ip_request_status_is_200(my_ip):
    assert my_ip.status_code == 200


def test_my_ip_request_has_a_body(my_ip):
    data = my_ip.json()
    assert data is not None
    assert data['ip'] is not None


def test_my_ip_has_json_header(my_ip):
    assert my_ip.headers["Content-Type"] == "application/json"

