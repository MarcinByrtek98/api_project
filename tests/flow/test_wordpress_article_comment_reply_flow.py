import time
from base64 import b64encode
import requests
import pytest
import lorem

username_editor = "editor"
password_editor = "HWZg hZIP jEfK XCDE V9WM PQ3t"
email_editor = "editor@somesite.com"
token_editor = b64encode(f"{username_editor}:{password_editor}".encode('utf-8')).decode("ascii")

username_commenter = "commenter"
password_commenter = "SXlx hpon SR7k issV W2in zdTb"
email_commenter = "commenter@somesite.com"
token_commenter = b64encode(f"{username_commenter}:{password_commenter}".encode('utf-8')).decode("ascii")

blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post " + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope="module")
def comment_commenter():
    comment = {
        "author_name": "commenter",
        "author_email": email_commenter,
        "content": "This is comment to the article.  " + lorem.sentence()
    }
    return comment


@pytest.fixture(scope="module")
def comment_editor():
    comment = {
        "author_name": "editor",
        "author_email": email_editor,
        "content": "This is reply to the comment.  " + lorem.sentence()
    }
    return comment


@pytest.fixture(scope='module')
def headers_editor():
    return {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_editor
    }


@pytest.fixture(scope='module')
def headers_commenter():
    return {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_commenter
    }


@pytest.fixture(scope='module')
def posted_article(article, headers_editor):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=headers_editor, json=payload)
    return response


@pytest.fixture(scope="module")
def posted_comment(comment_commenter, headers_commenter, posted_article):
    parent_article_id = posted_article.json()["id"]
    payload = {
        "author_name": comment_commenter["author_name"],
        "author_email": comment_commenter["author_email"],
        "content": comment_commenter["content"],
        "status": "publish",
        "post": parent_article_id
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_commenter, json=payload)
    return response


@pytest.fixture(scope="module")
def answer_editor_comment(posted_comment, headers_editor, comment_editor, posted_article):
    parent_comment_id = posted_comment.json()["id"]
    parent_article = posted_article.json()["id"]
    payload = {
        "author_name": comment_editor["author_name"],
        "author_email": comment_editor["author_email"],
        "content": comment_editor["content"],
        "status": "publish",
        "post": parent_article,
        "parent": parent_comment_id
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_editor, json=payload)
    return response


def test_create_new_post_successfully(posted_article):
    # Weryfikacja Powodzenia operacji CREATE
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


def test_verify_authorship_of_new_post(article, posted_article):
    # Weryfikacja Autorstwa artykułu
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 200
    assert published_article.reason == "OK"
    wordpress_post_data = published_article.json()
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{article["article_subtitle"]}</p>\n'
    assert wordpress_post_data["content"]["rendered"] == f'<p>{article["article_text"]}</p>\n'
    assert wordpress_post_data["status"] == 'publish'
    assert wordpress_post_data["author"] == 2


def test_create_new_comment_successfully(posted_comment, posted_article):
    # Weryfikacja Powodzenia operacji CREATE
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"


def test_verify_relation_between_post_and_comment(posted_comment, posted_article):
    # Weryfikacja Relacji między postem a komentarzem
    assert posted_comment.json()["post"] == posted_article.json()["id"]


def test_verify_authorship_of_new_comment(posted_comment, comment_commenter):
    # Weryfikacja Autorstwa komentarza
    wordpress_comment_url = f'{comments_endpoint_url}/{posted_comment.json()["id"]}'
    published_comment = requests.get(wordpress_comment_url)
    wordpress_comment_data = published_comment.json()
    assert published_comment.status_code == 200
    assert published_comment.reason == "OK"
    assert wordpress_comment_data["author_name"] == comment_commenter["author_name"]
    assert wordpress_comment_data["content"]["rendered"] == f'<p>{comment_commenter["content"]}</p>\n'
    assert wordpress_comment_data["author"] == posted_comment.json()["author"]


def test_create_new_answer_successfully(answer_editor_comment):
    # ZWERYFIKUJ: Powodzenie operacji CREATE
    assert answer_editor_comment.status_code == 201
    assert answer_editor_comment.reason == "Created"


def test_verify_relation_between_comment_and_answer(answer_editor_comment, posted_comment):
    # Weryfikacja: Relacji pomiędzy komentarzem a odpowiedzią
    assert answer_editor_comment.json()["parent"] == posted_comment.json()["id"]


def test_verify_relation_between_answer_and_article(answer_editor_comment, posted_article):
    # Weryfikacja: Relacji pomiędzy odpowiedzią a artykułem
    assert answer_editor_comment.json()["post"] == posted_article.json()["id"]


def test_verify_authorship_of_answer(comment_editor, answer_editor_comment):
    # Weryfikacja: Autorstwa odpowiedzi
    assert answer_editor_comment.json()["author_name"] == comment_editor["author_name"]