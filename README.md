# Flow Testing with Python

This repository demonstrates flow testing using Python and the WordPress REST API. The purpose of these tests is to validate the flow of creating a new article, adding a comment to the article and replying to the comment.

## Environment

- Base URL: https://gaworski.net
- Editor's Username: editor
- Editor's Email: editor@somesite.com
- Editor's Application Password: HWZg hZIP jEfK XCDE V9WM PQ3t
- Commenter's Username: commenter
- Commenter's Email: commenter@somesite.com
- Commenter's Application Password: SXlx hpon SR7k issV W2in zdTb

## Documentation

- WordPress API: [https://developer.wordpress.org/rest-api](https://developer.wordpress.org/rest-api)
- API Reference: [https://developer.wordpress.org/rest-api/reference](https://developer.wordpress.org/rest-api/reference)

## Scenario

1. Add a new article as the editor user
    - VERIFY: Successful CREATE operation
    - VERIFY: Article authorship

2. Add a comment to the article created in step 1 as the commenter user
    - VERIFY: Successful CREATE operation
    - VERIFY: Relationship between the post and the comment
    - VERIFY: Comment authorship

3. Add a reply to the comment created in step 2 as the editor user
    - VERIFY: Successful CREATE operation
    - VERIFY: Relationship between the comment and the reply
    - VERIFY: Relationship between the reply and the article
    - VERIFY: Reply authorship

## Usage

1. Install the required dependencies:
   ```bash
   pip install requests
